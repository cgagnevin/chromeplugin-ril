/**
 * Created by Cedric on 22/03/2016.
 */
/****************************************
 *             Configuration            *
 ****************************************/

var config = {
    baseUrl: 'http://127.0.0.1:8000/api/posts/',
};

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
	
	//Display the waiting icon
	chrome.browserAction.setIcon({
		path : {
			"19": "img/icon_wait19.png"
	  }
	});
	
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		var activeTab = tabs[0];
		
		/**
		* Perform a request to the RIL API in order to save the current page
		*/	
		$.ajax({
			type: "POST",
			url: config.baseUrl,
			data: { "post[title]": activeTab.title, "post[url]": activeTab.url}
		})
		.done(function(msg) {
			chrome.browserAction.setIcon({
				path : {
					"19": "img/icon_ok19.png"
			  }
			});
		})
		.fail(function( jqXHR, textStatus ) {
			chrome.browserAction.setIcon({
				path : {
					"19": "img/icon_ko19.png"
			  }
			});
		})
		.always(function() {
			setTimeout(
				function() 
				{
					chrome.browserAction.setIcon({
						path : {
							"19": "img/icon19.png"
					  }
					});
				}, 5000);;
		});
	});
});
